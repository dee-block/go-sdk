package types

const (
	DefaultApiSchema        = "https"
	DefaultWSSchema         = "wss"
	DefaultAPIVersionPrefix = "/api/v1"
	DefaultWSPrefix         = "/api/ws"
	NativeSymbol            = "DEE"

	ProdChainID    = "DeeChain-Main"
	TestnetChainID = "DeeChain-Testnet"
	KongoChainId   = "DeeChain-Testnet2"
	GangesChainId  = "DeeChain-Testnet3"

	RialtoNet = "rialto"
	ChapelNet = "chapel"
)
