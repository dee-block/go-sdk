package types

import (
	"github.com/cosmos/cosmos-sdk/types"
)

type AccAddress = types.AccAddress

type ChainNetwork uint8

const (
	TestNetwork ChainNetwork = iota
	ProdNetwork
	TmpTestNetwork
	GangesNetwork
)

const (
	AddrLen = types.AddrLen
)

var Network = ProdNetwork

func SetNetwork(network ChainNetwork) {
	Network = network
	if network != ProdNetwork {
		sdkConfig := types.GetConfig()
		sdkConfig.SetBech32PrefixForAccount("tdee", "deep")
	}
}

func (this ChainNetwork) Bech32Prefixes() string {
	switch this {
	case TestNetwork:
		return "tdee"
	case TmpTestNetwork:
		return "tdee"
	case GangesNetwork:
		return "tdee"
	case ProdNetwork:
		return "dee"
	default:
		panic("Unknown network type")
	}
}

func init() {
	sdkConfig := types.GetConfig()
	sdkConfig.SetBech32PrefixForAccount("dee", "deep")
	sdkConfig.SetBech32PrefixForValidator("dva", "dvap")
	sdkConfig.SetBech32PrefixForConsensusNode("dca", "dcap")
}

var (
	AccAddressFromHex    = types.AccAddressFromHex
	AccAddressFromBech32 = types.AccAddressFromBech32
	GetFromBech32        = types.GetFromBech32
	MustBech32ifyConsPub = types.MustBech32ifyConsPub
	Bech32ifyConsPub     = types.Bech32ifyConsPub
	GetConsPubKeyBech32  = types.GetConsPubKeyBech32
)
